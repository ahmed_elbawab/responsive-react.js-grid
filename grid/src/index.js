import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import registerServiceWorker from "./registerServiceWorker";

import Header from "./components/header.jsx";
import Root from "./components/root.jsx";
import Footer from "./components/footer.jsx";

import "bootstrap/dist/css/bootstrap.css";

ReactDOM.render(<Header />, document.getElementById("head"));
ReactDOM.render(<Root />, document.getElementById("root"));
ReactDOM.render(<Footer />, document.getElementById("footer"));
registerServiceWorker();
