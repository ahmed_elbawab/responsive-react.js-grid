import React, { Component } from "react";
import "./footer.css";

class Footer extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <div className="footer-container">
          <p>2018 &#169; All rights are reserved</p>
        </div>
      </React.Fragment>
    );
  }
}

export default Footer;
