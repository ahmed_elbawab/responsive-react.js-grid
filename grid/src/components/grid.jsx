import React, { Component } from "react";
import "./grid.css";
import Body from "./gridBody";
import AddItem from "./addButton";

class Grid extends Component {
  state = {
    tableHeaderData: {
      name: "Name",
      age: "Age",
      address: "Address",
      country: "Country",
      isMarried: "Is Married",
      tools: "Tools"
    }
  };
  render() {
    return (
      <React.Fragment>
        <table>
          <thead>
            <tr>
              <th>
                <div>
                  {this.state.tableHeaderData.name}
                  <i
                    class="fas fa-chevron-up"
                    onClick={this.props.handelNameUp}
                  />
                  <i
                    class="fas fa-chevron-down"
                    onClick={this.props.handelNameDown}
                  />
                </div>
              </th>
              <th>
                <div>
                  {this.state.tableHeaderData.age}
                  <i
                    class="fas fa-chevron-up"
                    onClick={this.props.handelAgeUp}
                  />
                  <i
                    class="fas fa-chevron-down"
                    onClick={this.props.handelAgeDown}
                  />
                </div>
              </th>
              <th>
                <div>{this.state.tableHeaderData.address}</div>
              </th>
              <th>
                <div>{this.state.tableHeaderData.country}</div>
              </th>
              <th>
                <div>{this.state.tableHeaderData.isMarried}</div>
              </th>
              <th>
                <div className="trash-container">
                  <i
                    className="fas fa-trash"
                    onClick={this.props.handelDeleteAll}
                  />
                  <i class="fas fa-search" onClick={this.props.handelSearch} />
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            <Body
              rowsData={this.props.rowsData}
              handelDelete={this.handelDeleteButton}
              handelCheck={this.handelCheckBox}
              handelChange={this.handelChangeData}
            />
          </tbody>
        </table>
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
        />
        <link
          rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
          integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ"
          crossOrigin="anonymous"
        />
        <AddItem addItem={this.addItem} />
      </React.Fragment>
    );
  }

  handelDeleteButton = id => {
    let temp = this.props.rowsData;
    temp = temp.filter(c => c.id !== id);
    this.props.handelDelete(temp);
  };

  handelCheckBox = id => {
    let temp = this.props.rowsData;
    for (let i = 0; i < temp.length; i++) {
      if (temp[i].id == id) {
        temp[i].isMarried = !temp[i].isMarried;
        break;
      }
    }
    this.props.handelCheck(temp);
  };

  handelChangeData = (id, type, value) => {
    let temp = this.props.rowsData;
    for (let i = 0; i < temp.length; i++) {
      if (temp[i].id == id) {
        if (type === "name") {
          temp[i].name = value;
        } else if (type === "age") {
          temp[i].age = value;
        } else if (type === "address") {
          temp[i].address = value;
        } else {
          temp[i].country = value;
        }
      }
    }
    this.props.handelChange(temp);
  };

  addItem = () => {
    let temp = this.props.rowsData;
    let id = 0;
    if (temp.length === 0) {
      id = 0;
    } else {
      id = temp[temp.length - 1].id + 1;
    }
    const item = {
      id: id,
      name: "please add name here",
      age: "please add age here",
      address: "please add address here",
      country: "please add country here",
      isMarried: false
    };
    temp.push(item);
    this.props.handelAdd(temp);
  };
}

export default Grid;
