import React, { Component } from "react";
import "./pages.css";

class Pages extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <div className="pages-container">
          {this.props.pagesArray.map(pageArray => (
            <div
              key={pageArray.pageNmber}
              className="page-number"
              onClick={() => this.props.changePageContent(pageArray.pageNmber)}
            >
              {pageArray.pageNmber}
            </div>
          ))}
        </div>
      </React.Fragment>
    );
  }
}

export default Pages;
