import React, { Component } from "react";
import "./row.css";
import Cell from "./cell.jsx";

class Row extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <td>
          <Cell
            value={this.props.name}
            type="name"
            handelChange={this.handelNameChange}
          />
        </td>
        <td>
          <Cell
            value={this.props.age}
            type="age"
            handelChange={this.handelNameChange}
          />
        </td>
        <td>
          <Cell
            value={this.props.address}
            type="address"
            handelChange={this.handelNameChange}
          />
        </td>
        <td>
          <Cell
            value={this.props.country}
            type="country"
            handelChange={this.handelNameChange}
          />
        </td>
        <td>
          <input
            type="checkbox"
            checked={this.props.isMarried}
            onChange={() => this.props.handelCheck(this.props.id)}
          />
        </td>
        <td>
          <i
            className="far fa-trash-alt"
            onClick={() => this.props.handelDelete(this.props.id)}
          />
        </td>
      </React.Fragment>
    );
  }

  handelNameChange = (type, value) => {
    this.props.handelChange(this.props.id, type, value);
  };
}

export default Row;
