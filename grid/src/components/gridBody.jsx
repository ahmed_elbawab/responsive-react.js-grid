import React, { Component } from "react";
import Row from "./row.jsx";
import "./gridBody.css";

class Body extends Component {
  state = {};

  render() {
    return (
      <React.Fragment>
        {this.props.rowsData.map(rowElement => (
          <tr
            id={"rowElement"}
            key={rowElement.id}
            style={this.setRowStyle(this.props.rowsData.indexOf(rowElement))}
          >
            {
              <Row
                id={rowElement.id}
                name={rowElement.name}
                age={rowElement.age}
                address={rowElement.address}
                country={rowElement.country}
                isMarried={rowElement.isMarried}
                handelDelete={this.props.handelDelete}
                handelCheck={this.props.handelCheck}
                handelChange={this.props.handelChange}
              />
            }
          </tr>
        ))}
      </React.Fragment>
    );
  }

  setRowStyle = id => {
    if (id % 2 === 0) {
      let rowStyle = {
        background: "white"
      };
      return rowStyle;
    } else {
      let rowStyle = {
        background: "#d0d8e0"
      };
      return rowStyle;
    }
  };
}

export default Body;
