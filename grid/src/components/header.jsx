import React, { Component } from "react";
import logo from "../resources/logo.jpg";
import "./header.css";

class Header extends Component {
  state = {
    time: "00:00:00"
  };
  render() {
    setInterval(this.setTime, 1000);
    return (
      <div className="Header">
        <header className="Header-header">
          <img src={logo} className="Header-logo" alt="logo" />
          <h1 className="Header-title">Welcome to GOGO Grid Controller</h1>
          <p>{this.state.time}</p>
        </header>
      </div>
    );
  }

  setTime = () => {
    let d = new Date();
    this.setState({
      time: d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds()
    });
  };
}

export default Header;
