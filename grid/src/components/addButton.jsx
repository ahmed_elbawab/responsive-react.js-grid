import React, { Component } from "react";
import "./addButton.css";

class AddButton extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <div className="float" onClick={() => this.props.addItem()}>
          <i className="fa fa-plus my-float" />
        </div>
      </React.Fragment>
    );
  }
}

export default AddButton;
