import React, { Component } from "react";
import EditableLabel from "./Assests/react-inline-editing/node_modules/react-inline-editing";

class Cell extends Component {
  constructor(props) {
    super(props);

    this._handleFocus = this._handleFocus.bind(this);
    this._handleFocusOut = this._handleFocusOut.bind(this);
  }

  _handleFocus(text) {}

  _handleFocusOut(text) {
    this.props.handelChange(this.props.type, text);
  }
  state = {};
  render() {
    return (
      <React.Fragment>
        <EditableLabel
          text={this.props.value}
          onFocus={this._handleFocus}
          onFocusOut={this._handleFocusOut}
        />
      </React.Fragment>
    );
  }
}

export default Cell;
