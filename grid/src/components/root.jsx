import React, { Component } from "react";
import Grid from "./grid.jsx";
import Pages from "./pages";

class Root extends Component {
  state = {
    pages: [
      {
        pageNmber: 1,
        data: [
          {
            id: 0,
            name: "Ahmed",
            age: "21",
            address: "50 gogo st",
            country: "usa",
            isMarried: false
          },
          {
            id: 1,
            name: "Mohamed",
            age: "32",
            address: "25 lala st",
            country: "spain",
            isMarried: true
          },
          {
            id: 2,
            name: "Tamer",
            age: "45",
            address: "30 home alex",
            country: "Egypt",
            isMarried: false
          },
          {
            id: 3,
            name: "Kamal",
            age: "26",
            address: "tanta",
            country: "france",
            isMarried: true
          },
          {
            id: 4,
            name: "Ahmed",
            age: "21",
            address: "50 gogo st",
            country: "usa",
            isMarried: false
          },
          {
            id: 5,
            name: "Mohamed",
            age: "32",
            address: "25 lala st",
            country: "spain",
            isMarried: true
          },
          {
            id: 6,
            name: "Tamer",
            age: "45",
            address: "30 home alex",
            country: "Egypt",
            isMarried: false
          },
          {
            id: 7,
            name: "Kamal",
            age: "26",
            address: "tanta",
            country: "france",
            isMarried: true
          }
        ]
      },
      {
        pageNmber: 2,
        data: [
          {
            id: 0,
            name: "Ahmed",
            age: "32",
            address: "25 lala st",
            country: "spain",
            isMarried: true
          },
          {
            id: 1,
            name: "Mohamed",
            age: "32",
            address: "25 lala st",
            country: "spain",
            isMarried: true
          }
        ]
      },
      {
        pageNmber: 3,
        data: [
          {
            id: 0,
            name: "Ahmed",
            age: "32",
            address: "25 lala st",
            country: "spain",
            isMarried: true
          },
          {
            id: 1,
            name: "Tarek",
            age: "32",
            address: "25 lala st",
            country: "spain",
            isMarried: true
          },
          {
            id: 2,
            name: "Ziad",
            age: "32",
            address: "25 lala st",
            country: "spain",
            isMarried: true
          },
          {
            id: 3,
            name: "LALA",
            age: "32",
            address: "25 lala st",
            country: "spain",
            isMarried: true
          }
        ]
      },
      {
        pageNmber: 4,
        data: [
          {
            id: 0,
            name: "Lala",
            age: "32",
            address: "25 lala st",
            country: "spain",
            isMarried: true
          }
        ]
      }
    ],

    currentPage: 1,

    rowsData: [
      {
        id: 0,
        name: "Ahmed",
        age: "21",
        address: "50 gogo st",
        country: "usa",
        isMarried: false
      },
      {
        id: 1,
        name: "Mohamed",
        age: "32",
        address: "25 lala st",
        country: "spain",
        isMarried: true
      },
      {
        id: 2,
        name: "Tamer",
        age: "45",
        address: "30 home alex",
        country: "Egypt",
        isMarried: false
      },
      {
        id: 3,
        name: "Kamal",
        age: "26",
        address: "tanta",
        country: "france",
        isMarried: true
      },
      {
        id: 4,
        name: "Ahmed",
        age: "21",
        address: "50 gogo st",
        country: "usa",
        isMarried: false
      },
      {
        id: 5,
        name: "Mohamed",
        age: "32",
        address: "25 lala st",
        country: "spain",
        isMarried: true
      },
      {
        id: 6,
        name: "Tamer",
        age: "45",
        address: "30 home alex",
        country: "Egypt",
        isMarried: false
      },
      {
        id: 7,
        name: "Kamal",
        age: "26",
        address: "tanta",
        country: "france",
        isMarried: true
      }
    ]
  };
  render() {
    return (
      <React.Fragment>
        <Grid
          rowsData={this.state.rowsData}
          handelDelete={this.handelDeleteButton}
          handelCheck={this.handelCheckBox}
          handelChange={this.handelChangeData}
          handelAdd={this.handelAddItem}
          handelDeleteAll={this.handelDeleteAllData}
          handelSearch={this.handelSearchButton}
          handelNameUp={this.handelNameSortUp}
          handelNameDown={this.handelNameSortDown}
          handelAgeUp={this.handelAgeUp}
          handelAgeDown={this.handelAgeDown}
        />
        <Pages
          pagesArray={this.state.pages}
          changePageContent={this.changePage}
        />
      </React.Fragment>
    );
  }

  handelDeleteButton = newData => {
    let temp = this.state.pages;
    temp[this.state.currentPage - 1].data = newData;
    this.setState({ pages: temp });
    this.setState({ rowsData: newData });
  };

  handelCheckBox = newData => {
    let temp = this.state.pages;
    temp[this.state.currentPage - 1].data = newData;
    this.setState({ pages: temp });
    this.setState({ rowsData: newData });
  };

  handelChangeData = newData => {
    let temp = this.state.pages;
    temp[this.state.currentPage - 1].data = newData;
    this.setState({ pages: temp });
    this.setState({ rowsData: newData });
  };

  handelAddItem = newData => {
    let temp = this.state.pages;
    temp[this.state.currentPage - 1].data = newData;
    this.setState({ pages: temp });
    this.setState({ rowsData: newData });
  };

  changePage = num => {
    this.setState({ currentPage: num });
    let temp = this.state.pages[num - 1];
    this.setState({ rowsData: temp.data });
  };

  handelDeleteAllData = () => {
    let temp = this.state.pages;
    temp[this.state.currentPage - 1].data = [];
    this.setState({ pages: temp });
    this.setState({ rowsData: [] });
  };

  handelSearchButton = () => {
    let item = prompt("What do you want to search about?");
    if (item !== null) {
      let temp = this.state.pages[this.state.currentPage].data;
      let newData = [];
      for (let i = 0; i < temp.length; i++) {
        if (temp[i].name === item) {
          newData.push(temp[i]);
        } else if (temp[i].age === item) {
          newData.push(temp[i]);
        } else if (temp[i].address === item) {
          newData.push(temp[i]);
        } else if (temp[i].country === item) {
          newData.push(temp[i]);
        }
      }
      this.setState({ rowsData: newData });
    }
  };

  handelNameSortUp = () => {};

  handelNameSortDown = () => {};

  handelAgeSortUp = () => {};

  handelAgeSortDown = () => {};
}

export default Root;
